# code test를 위한 node express 환경 구성


### package.json 생성

    npm init -y

### express 설치

    npm install express

### ejs 설치

    npm install ejs

### /server.js 추가

    // Express 서버생성
    const express = require('express');
    const app = express();
    
    // 서버가 읽을 수 있도록 HTML의 위치를 정의해준다
    app.set('views', __dirname + '/views');
    const router = require('./router/main')(app);
    
    // 서버가 HTML 렌더링을 할 때, EJS엔진을 사용하도록 설정한다
    app.set('view engine', 'ejs');
    app.engine('html', require('ejs').renderFile);
    
    const server = app.listen(3000, function(){
    console.log("Express server has started on port 3000")
    });
    
    // 정적파일 경로 설정하기
    app.use(express.static(__dirname + '/public'));ㄹ

### /main.js 추가
root에 router 폴더 생성 후 main.js 파일을 추가한다.(router 역활)    

    module.exports = function (app) {
        console.log("START code test")
        
        app.get('/', function (req, res) {
            res.render('index.html')
        });
    }

### index.html 추가

    <!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
        <meta name="author" content="TSK">
        <meta name="description" content="code test를 위한 환경구성">
        <meta name="keywords" content="코드테스트, code test">
        <meta property="og:type" content="website">
        <meta property="og:title" content="<%= title %>">
        <meta property="og:description" content="code test를 위한 환경구성">
        <meta property="og:image" content="https://finiview.com/images/common/meta-logo.png">
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="200" />
        <meta property="og:url" content="https://www.notion.so/kimjosa/express-2472c5fb74e34515a1ae685ae69d7d3a">
    
        <title><%= title %></title>
    	  <link rel="canonical" href="https://www.notion.so/kimjosa/express-2472c5fb74e34515a1ae685ae69d7d3a">
    	  <link rel="shortcut icon" sizes="192x192" href="/images/favicon/mobile_img.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/mobile_img.png">
        <link rel="stylesheet" href="/css/layout.css" media="screen, print">
      </head>
    
      <body>
        <noscript>
        </noscript>
        <div id="app">
    	    <h1><%= title %></h1>
    	    <p>Welcome to <%= title %></p>
        </div>
        <!-- built files will be auto injected -->
      </body>
    </html>

---

### watch를 이용하여 scss 파일 자동 컴파일하기
package.json에 등록하고 npm script로 간단하게 사용하자.
한번 실행하면 저장할때마다 자동으로 컴파일 해 준다.

    "watch:sass": "sass --watch src/public/sass/layout.scss src/public/css/layout.css"
    
    // sass 폴더의 파일을 일괄적으로 css 폴더로 컴파일 한다.
    "watch:sass": "sass --watch src/public/sass:src/public/css"

### nodemon 설치
node가 실행하는 파일이 속한 디렉토리를 감시하다가 파일이 수정되면 자동으로 노드를 재시작하게 해준다.

    npm install --save-dev nodemon

package.json 수정

    "scripts": {
    	"start": "node ./bin/www"
    }

    "scripts": {  
        "start": "nodemon ./bin/www"  // npm start를 nodemon으로 실행하게 수정
    }
    
### live reload 적용
node express환경에서 live reload를 적용해 다.
vue.js에서 가장 편했던 기능중 하나이다.

    npm i livereload connect-livereload
    // livereload 패키지와 connect-livereload 패키지 설치

#### app.js 소스 추가 

    import express from 'express';
    import livereload from 'livereload';
    import livereloadMiddleware from 'connect-livereload';
    
    // 라이브 서버 설정
    const liveServer = livereload.createServer({
        // 변경시 다시 로드할 파일 확장자들 설정
        exts: ['html', 'css', 'ejs'],
        debug: true
    });
    
    liveServer.watch(__dirname);
    
    ...
    ...
    
    const app = express();
    
    ...
    ...
    
    // 미들웨어 설정
    app.use(livereloadMiddleware());
    
    ...
    ...
