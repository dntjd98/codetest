const createError = require('http-errors');
// Express 서버생성
const express = require('express');

const livereload = require('livereload');
const livereloadMiddleware = require('connect-livereload');
// import livereload from 'livereload';
// import livereloadMiddleware from 'connect-livereload';

// 라이브 서버 설정
const liveServer = livereload.createServer({
    // 변경시 다시 로드할 파일 확장자들 설정
    exts: ['html', 'css', 'ejs', 'jpg', 'png'],
    debug: true
});

liveServer.watch(__dirname);

const app = express();

const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');

const indexRouter = require('./routes');
const usersRouter = require('./routes/users');

// 서버가 읽을 수 있도록 HTML의 위치를 정의해준다
app.set('views', path.join(__dirname, 'views'));

// 서버가 HTML 렌더링을 할 때, EJS엔진을 사용하도록 설정한다
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
	src: path.join(__dirname, 'public'),
	dest: path.join(__dirname, 'public'),
	indentedSyntax: true, // true = .sass and false = .scss
	sourceMap: true
}));

// 정적파일 경로 설정하기
app.use(express.static(path.join(__dirname, 'public')));
// 미들웨어 설정
app.use(livereloadMiddleware());

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
	next(createError(404));
});

// error handler
app.use(function (err, req, res) {
	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};
	
	// render the error page
	res.status(err.status || 500);
	res.render('error');
});

module.exports = app;
