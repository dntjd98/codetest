// 현재 브라우저 노출
document.documentElement.setAttribute("data-useragent", navigator.userAgent);

$(function () {
	// 인풋박스 초기화
	$('.btn-input-del').on('click', function () {
		$(this).closest('.input-label-group').find('input').val('').focus();
	})

	// 비밀번호 보기
	$('.login-form .btn-mask').on('click', function () {
		if ($(this).hasClass('on')) {
			$(this).removeClass('on');
			$(this).closest('.input-label-group').find('input').get(0).type = 'text';
		} else {
			$(this).addClass('on');
			$(this).closest('.input-label-group').find('input').get(0).type = 'password';
		}
		return false;
	});
});

// placeholder대체용 label on/off
$(function () {
	$('input').on('input', $(this), function () {
		$(this).off('keyup.setPlaceholderEvent').on('keyup.setPlaceholderEvent', function () {
			if ($(this).val().length) {
				$(this).prev('.label-holder').addClass('label-off');
			} else {
				$(this).prev('.label-holder').removeClass('label-off');
			}
		});
	});
});

// modal 호출
$(function multiModal() {
	$('a[data-modal]').click(function () {
		// var url = $.attr(this, 'href');
		// window.location.hash = url;

		$(this).modal({
			fadeDuration: 100,
			closeExisting: false //다중모달 사용
		});
		return false;
	});
});

//tab-menu 설정
$(function tabMenu() {
	$('.tab-conts').hide();
	$('.tab>ul>li').click(function (e) {
		e.preventDefault();
		var dataId = $(this).data('id');
		
		$(this).parents('.wrap-tab').find('.tab-conts').each(function () { 
			$(this).removeClass('on').hide(); 
		});
		$(this).parents('.wrap-tab').find('#' + dataId).addClass('on').fadeIn(300);
		$(this).siblings('li').removeClass('on');
		$(this).addClass('on');
		// console.log(this);
	});
});

// file 첨부
// $(function () {
// 	var fileTarget = $('.file-box .hidden-file');
// 	fileTarget.on('change', function () {
// 		if (window.FileReader) {
// 			var filename = $(this)[0].files[0].name;
// 		} else {
// 			var filename = $(this).val().split('/').pop().split('\\').pop();
// 		}

// 		$(this).siblings('.file-name').val(filename);
// 	});
// });

// file 첨부
$(function fileAddon() {
	var fileTarget = $('.file-box .hidden-file');
	fileTarget.on('change', function () {
		if (window.FileReader) {
			var filename = $(this)[0].files[0].name;
		} else {
			var filename = $(this).val().split('/').pop().split('\\').pop();
		}
		$(this).siblings('.file-name').val(filename);
		$(this).closest('.file-box').find('.label-holder').hide();
	});
});


// 스크롤 탑
$(function () {
	$(window).scroll(function () {
		if ($(this).scrollTop() > 300) {
			$('.scroll-top').fadeIn('500');
		} else {
			$('.scroll-top').fadeOut('fast');
		}
	});
	$('.btn-top-scroll').click(function (e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, 400);
	});
});

// drop-menu 
$(function () {
	$('.drop-menu').hover(
		function () {
			$(this).addClass('on');
			$(this).children('ul').fadeIn(250);
		},
		function () {
			$(this).removeClass('on');
			$(this).children('ul').hide();
		}
	);
});

// 고객센터 drop-menu 
$(function csCenter() {
	$('.click-drop-menu').click(function () {
		$(this).toggleClass('on');
		$(this).children('ul').fadeToggle(0);
	});
});

$(function postScrap() {
	$('.scrap.board').click(function () {
		$('.scrap.board').toggleClass('on');
		$('.tooltip-text.scrap').toggleClass('on');
		var tooltipTimer = $('.tooltip-text.scrap').addClass('tooltip-inherit');
		setTimeout(function () {
			$('.tooltip-text.scrap').removeClass('tooltip-inherit');
		}, 2000);
		clearTimeout(tooltipTimer);
	});
});

// custom scrollbar
$(function scrollBar() {
	$('.select-box-list').mCustomScrollbar({
		theme: 'dark',
		scrollInertia: 400
	});
	$('.custom-scroll').mCustomScrollbar({
		theme: 'dark',
		scrollInertia: 400
	});
});


// selectbox
$(function selectBox() {
	$(document).on('click', function (e) {
		if (!$(e.target).is('.select-box-current, .select-box-current > .inner')) {
			$('.select-box-current').removeClass('on');
			$('.select-box-current').closest('.select-box').removeClass('on');
		}
	});

	$(document).on('click', '.select-box-current', function () {
		$('body').find('.select-box-current').not(this).removeClass('on');
		$('body').find('.select-box-current').not(this).closest('.select-box').removeClass('on');
		$(this).closest('.select-box').toggleClass('on');
		$(this).toggleClass('on');
	});

	$(document).on('click', '.select-box-list li', function () {
		var optionHtml = $(this).html();
		$(this).closest('.select-box').find('.select-box-current > .inner').html(optionHtml).removeClass('on');
	});
});