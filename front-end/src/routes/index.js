var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.html', { title: 'Express - code test' });
});

router.get('/scroll', function(req, res, next) {
  res.render('scrollTransform.html', { title: 'scroll-transform - code test' });
});

router.get('/template', function(req, res, next) {
  res.render('template.html', { title: 'coding-convention - code test' });
});

module.exports = router;
