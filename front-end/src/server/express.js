import express from "express"
import path from "path"
// const express = require('express');
// const path = require('path');

const server = express()
const webpack = require('webpack')

// webpack의 output 장소인 dist를 express static으로 등록한다.
const staticMiddleWare = express.static("dist");

// webpack 설정
const config = require("../../config/webpack.dev.js")
const compiler = webpack(config);

// 웹팩을 미들웨어로 등록해서 사용하기 위한 모듈
const webpackDevMiddleware = require("webpack-dev-middleware")(
    compiler,
    config.devServer
)

// 핫 로딩 미들웨어를 추가하자!
const webpackHotMiddleware =
    require("webpack-hot-middleware")(compiler)

server.use(webpackDevMiddleware);
 // 웹팩dev 미들웨어 다음, static 미들웨어 이전
server.use(webpackHotMiddleware);
server.use(staticMiddleWare);

server.listen(8080, () => {
    console.log("Server is Listening")
});